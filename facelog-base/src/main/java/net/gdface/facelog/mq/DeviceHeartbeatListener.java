package net.gdface.facelog.mq;

import gu.simplemq.IMessageAdapter;

/**
 * 设备心跳侦听器
 * @author guyadong
 *
 */
public interface DeviceHeartbeatListener extends IMessageAdapter<DeviceHeartdbeatPackage> {

}
