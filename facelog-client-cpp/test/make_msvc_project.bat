echo off 
echo make facelogclient VS2015 project
if not defined VS140COMNTOOLS (
	echo vs2015 NOT FOUND.
	exit /B -1
)
echo vs2015 found.
where cmake
if errorlevel 1 (
	echo cmake NOT FOUND.
	exit /B -1
)
echo cmake found
set sh_folder=%~dp0
rem 删除最后的 '\'
set sh_folder=%sh_folder:~0,-1%
pushd %sh_folder%

if exist project.vs2015 rmdir project.vs2015 /s/q
mkdir project.vs2015
pushd project.vs2015
if not defined VisualStudioVersion (
	echo make MSVC environment ...
	call "%VS140COMNTOOLS%..\..\vc/vcvarsall" x86_amd64
)
echo creating x86_64 Project for Visual Studio 2015 ...
cmake -G "Visual Studio 14 2015 Win64" ^
	-DCMAKE_PREFIX_PATH=%sh_folder%/../release/facelogclient_windows_vc_x86_64;^
%sh_folder%/../dependencies/dist/thrift-dev/vc140/x64;^
	-DBOOST_ROOT=%sh_folder%/../dependencies/dist/boost_1_62_0^
	.. 

popd
popd