# FindPTHREADW32
# --------
#
# Find Pthread for win32
#
# Find the native pthread for win32 includes and library This module defines
#
# ::
#
#   PTHREADW32_INCLUDE_DIR, where to find pthread.h, etc.
#   PTHREADW32_LIBRARY, where to find the pthread for win32 library.
#   PTHREADW32_FOUND, If false, do not try to use pthread for win32.
#   PTHREADW32_DLL, dynamic library location,if exist
#		HAVE_PTHREAD, if true, pthread supported by compiler
#
#   IMPORTED TARGET: pthreadw32
#=============================================================================
if(NOT WIN32)
	return()
endif()
include(CheckLibraryExists)
CHECK_LIBRARY_EXISTS (pthread pthread_rwlock_init "" HAVE_PTHREAD)
if(HAVE_PTHREAD)
	message(STATUS "pthread supported")
	return()
endif()
find_path(PTHREADW32_INCLUDE_DIR pthread.h)
set(PTHREADW32_NAME pthread)
if(MSVC)
	set(PTHREADW32_NAME pthread.lib pthreadVC2)
elseif(MINGW)
	set(PTHREADW32_NAME libpthread.a pthreadGC2)	
endif()
if( CMAKE_SYSTEM_PROCESSOR MATCHES "x86_64|x64|AMD64|Win64")
	set(_arch  "x64")
elseif(CMAKE_SYSTEM_PROCESSOR MATCHES  "Win32|x86")
	set(_arch  "x86")
endif()

find_library(PTHREADW32_LIBRARY NAMES ${PTHREADW32_NAME} pthread PATH_SUFFIXES lib/${_arch} )

# handle the QUIETLY and REQUIRED arguments and set PTHREADW32_FOUND to TRUE if
# all listed variables are TRUE
include(FindPackageHandleStandardArgs)
FIND_PACKAGE_HANDLE_STANDARD_ARGS(PTHREADW32 DEFAULT_MSG PTHREADW32_LIBRARY PTHREADW32_INCLUDE_DIR)

mark_as_advanced(PTHREADW32_LIBRARY PTHREADW32_INCLUDE_DIR )
#message(STATUS PTHREADW32_INCLUDE_DIR=${PTHREADW32_INCLUDE_DIR})
if(PTHREADW32_FOUND) 
	if(MSVC)
		set(_dll_name pthreadVC2.dll)
	elseif(MINGW)
		set(_dll_name pthreadGC2.dll)	
	endif()
	find_file(PTHREADW32_DLL ${_dll_name} PATH_SUFFIXES dll/${_arch})
	#message(STATUS PTHREADW32_DLL=${PTHREADW32_DLL})
	
	add_library(pthreadw32 UNKNOWN IMPORTED)
	set_target_properties(pthreadw32 PROPERTIES
	  INTERFACE_INCLUDE_DIRECTORIES "${PTHREADW32_INCLUDE_DIR}"
	  IMPORTED_LINK_INTERFACE_LANGUAGES "C"
	  IMPORTED_LOCATION "${PTHREADW32_LIBRARY}"
	  )
	
	# 解决 Visual Studio 2015下编译struct timespec重定义问题
	if(MSVC)
		# 检查是否定义了 struct timespec
		include(CheckStructHasMember)
		CHECK_STRUCT_HAS_MEMBER("struct timespec" tv_sec time.h HAVE_STRUCT_TIMESPEC LANGUAGE C )  
		if(HAVE_STRUCT_TIMESPEC)
			set_target_properties(pthreadw32 PROPERTIES INTERFACE_COMPILE_DEFINITIONS HAVE_STRUCT_TIMESPEC )
		endif()
	endif()
else(PTHREADW32_FIND_REQUIRED)
	message(FATAL_ERROR  "NOT FOUND PTHREADW32")
endif()

unset(_arch)
