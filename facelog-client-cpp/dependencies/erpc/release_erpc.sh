#!/bin/bash
# 编译release版本
sh_folder=$(dirname $(readlink -f $0))
folder_name=$(basename $sh_folder) 

pushd $sh_folder

BUILD_TYPE=RELEASE \
./make_unix_makefile.sh || exit 255
cmake --build ../$folder_name.gnu --target clean
cmake --build ../$folder_name.gnu --target install -- -j8

popd
