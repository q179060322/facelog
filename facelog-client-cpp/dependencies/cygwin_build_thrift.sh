#!/bin/bash
# 安装编译所需的工具的依赖库
#apt-cyg install gcc-g++ automake autoconf diffutils libtool bison flex openssl-devel libglib2.0-devel
# 如果编译C++ Library(lib/cpp)需要安装
#apt-cyg install libboost-devel zlib-devel libevent-devel

sh_folder=$(dirname $(readlink -f $0))

[ -n "$1" ] && install_prefix=$1
[ -z "$1" ] && install_prefix=$sh_folder/dist/thrift_c_glib-$(arch)
[ -e "$install_prefix" ] && rm -fr "$install_prefix"

[ -n "$2" ] && thrift_folder=$2
[ -z "$2" ] && thrift_folder=thrift-0.11.0

pushd $thrift_folder

#./bootstrap.sh

# 生成Makefile
./configure  --enable-shared=no \
	--enable-tests=no \
	--enable-tutorial=no \
	--with-c_glib=yes \
	--with-cpp=no \
	--with-python=no \
	--with-java=no \
	--prefix=$install_prefix \
	CXXFLAGS="-D_GNU_SOURCE -DPTHREAD_MUTEX_RECURSIVE_NP=PTHREAD_MUTEX_RECURSIVE" || exit -1

make -j8 install

popd