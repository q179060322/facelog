#!/bin/bash
# 生成 IFaceLog erpc 代码脚本
sh_folder=$(dirname $(readlink -f $0))
pushd $sh_folder
OUT_FOLDER=$sh_folder/src/erpc_proxy_mini
# 指定 erpc compiler(erpcgen) 位置
which erpcgen >/dev/null 2>&1 && ERPCGEN_EXE=erpcgen
[ -z "$ERPCGEN_EXE" ] && echo "not found erpcgen.exe,please build erpcgen" && exit 255
ERPC_IDL=$sh_folder/../facelog-service/IFaceLog_mini.erpc
[ ! -f  "$ERPC_IDL" ] && echo "not found IDL file:$ERPC_IDL" && exit 255
if [ -d "$OUT_FOLDER" ]
then
	del  "$OUT_FOLDER/facelog.h*" >/dev/null 2>&1
	del  "$OUT_FOLDER/facelog_client.*" >/dev/null 2>&1
	del  "$OUT_FOLDER/facelog_server.*" >/dev/null 2>&1
fi
if [ ! -d "$OUT_FOLDER" ] 
then
	mkdir -p "$OUT_FOLDER" || exit 255
fi

echo ERPC_IDL=$ERPC_IDL
$ERPCGEN_EXE -o "$OUT_FOLDER" "$ERPC_IDL"
popd
