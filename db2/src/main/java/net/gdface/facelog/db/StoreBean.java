// ______________________________________________________
// Generated by sql2java - https://github.com/10km/sql2java 
// JDBC driver used at code generation time: com.mysql.jdbc.Driver
// template: bean.java.vm
// ______________________________________________________
package net.gdface.facelog.db;
import java.io.Serializable;
import java.util.Objects;

import gu.sql2java.BaseRow;

import com.facebook.swift.codec.ThriftStruct;
import com.facebook.swift.codec.ThriftField;
import com.facebook.swift.codec.ThriftField.Requiredness;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonIgnore;
import net.gdface.annotation.CodegenLength;
import net.gdface.annotation.CodegenRequired;
import net.gdface.annotation.CodegenInvalidValue;
/**
 * StoreBean is a mapping of fl_store Table.
 * <br>Meta Data Information (in progress):
 * <ul>
 *    <li>comments: 二进制数据存储表 </li>
 * </ul>
 * @author guyadong
*/
@ThriftStruct
@ApiModel(description="二进制数据存储表")
public final class StoreBean extends BaseRow
    implements Serializable,Constant
{
    private static final long serialVersionUID = -1684185165668832146L;

    /** comments:主键,md5检验码 */
    @ApiModelProperty(value = "主键,md5检验码" ,required=true ,dataType="String")
    @CodegenLength(value=32,prealloc=true)@CodegenRequired@CodegenInvalidValue
    private String md5;

    /** comments:编码类型,GBK,UTF8... */
    @ApiModelProperty(value = "编码类型,GBK,UTF8..."  ,dataType="String")
    @CodegenLength(max=16,prealloc=true)@CodegenInvalidValue
    private String encoding;

    /** comments:二进制数据(最大16MB) */
    @ApiModelProperty(value = "二进制数据(最大16MB)"  ,dataType="ByteBuffer")
    @CodegenLength(max=16777215)@CodegenInvalidValue
    private java.nio.ByteBuffer data;

    /** columns modified flag */
    @ApiModelProperty(value="columns modified flag",dataType="int",required=true)
    private int modified;
    /** columns initialized flag */
    @ApiModelProperty(value="columns initialized flag",dataType="int",required=true)
    private int initialized;
    /** new record flag  */
    @ApiModelProperty(value="new record flag",dataType="boolean",required=true)
    private boolean isNew;        
    @ThriftField(value=1,name="_new",requiredness=Requiredness.REQUIRED)
    @Override
    public boolean isNew()
    {
        return this.isNew;
    }

    /**
     * Specifies to the object if it has been set as new.
     *
     * @param isNew the boolean value to be assigned to the isNew field
     */
    @ThriftField()
    @Override
    public void setNew(boolean isNew)
    {
        this.isNew = isNew;
    }
    /**
     * @return the modified status of columns
     */
    @ThriftField(value=2,requiredness=Requiredness.REQUIRED)
    @Override
    public int getModified(){
        return modified;
    }

    /**
     * @param modified the modified status bit to be assigned to {@link #modified}
     */
    @ThriftField()
    @Override
    public void setModified(int modified){
        this.modified = modified;
    }
    /**
     * @return the initialized status of columns
     */
    @ThriftField(value=3,requiredness=Requiredness.REQUIRED)
    @Override
    public int getInitialized(){
        return initialized;
    }

    /**
     * @param initialized the initialized status bit to be assigned to {@link #initialized}
     */
    @ThriftField()
    @Override
    public void setInitialized(int initialized){
        this.initialized = initialized;
    }

    public StoreBean(){
        reset();
    }
    /**
     * construct a new instance filled with primary keys
     * @param md5 PK# 1 
     */
    public StoreBean(String md5){
        setMd5(md5);
    }
    /**
     * Getter method for {@link #md5}.<br>
     * PRIMARY KEY.<br>
     * Meta Data Information (in progress):
     * <ul>
     * <li>full name: fl_store.md5</li>
     * <li>comments: 主键,md5检验码</li>
     * <li>NOT NULL</li>
     * <li>column size: 32</li>
     * <li>JDBC type returned by the driver: Types.CHAR</li>
     * </ul>
     *
     * @return the value of md5
     */
    @ThriftField(value=4)
    @JsonProperty("md5")
    public String getMd5(){
        return md5;
    }
    /**
     * Setter method for {@link #md5}.<br>
     * The new value is set only if equals() says it is different,
     * or if one of either the new value or the current value is null.
     * In case the new value is different, it is set and the field is marked as 'modified'.
     *
     * @param newVal the new value( NOT NULL) to be assigned to md5
     */
    @ThriftField(name="md5")
    @JsonProperty("md5")
    public void setMd5(String newVal)
    {
        modified |= FL_STORE_ID_MD5_MASK;
        initialized |= FL_STORE_ID_MD5_MASK;

        if (Objects.equals(newVal, md5)) {
            return;
        }
        md5 = newVal;
    }
    /**
     * Determines if the md5 has been modified.
     *
     * @return true if the field has been modified, false if the field has not been modified
     */
    public boolean checkMd5Modified()
    {
        return 0 !=  (modified & FL_STORE_ID_MD5_MASK);
    }

    /**
     * Determines if the md5 has been initialized.<br>
     *
     * It is useful to determine if a field is null on purpose or just because it has not been initialized.
     *
     * @return true if the field has been initialized, false otherwise
     */
    public boolean checkMd5Initialized()
    {
        return 0 !=  (initialized & FL_STORE_ID_MD5_MASK);
    }
    /**
     * Getter method for {@link #encoding}.<br>
     * Meta Data Information (in progress):
     * <ul>
     * <li>full name: fl_store.encoding</li>
     * <li>comments: 编码类型,GBK,UTF8...</li>
     * <li>column size: 16</li>
     * <li>JDBC type returned by the driver: Types.VARCHAR</li>
     * </ul>
     *
     * @return the value of encoding
     */
    @ThriftField(value=5)
    @JsonProperty("encoding")
    public String getEncoding(){
        return encoding;
    }
    /**
     * Setter method for {@link #encoding}.<br>
     * The new value is set only if equals() says it is different,
     * or if one of either the new value or the current value is null.
     * In case the new value is different, it is set and the field is marked as 'modified'.
     *
     * @param newVal the new value to be assigned to encoding
     */
    @ThriftField(name="encoding")
    @JsonProperty("encoding")
    public void setEncoding(String newVal)
    {
        modified |= FL_STORE_ID_ENCODING_MASK;
        initialized |= FL_STORE_ID_ENCODING_MASK;

        if (Objects.equals(newVal, encoding)) {
            return;
        }
        encoding = newVal;
    }
    /**
     * Determines if the encoding has been modified.
     *
     * @return true if the field has been modified, false if the field has not been modified
     */
    public boolean checkEncodingModified()
    {
        return 0 !=  (modified & FL_STORE_ID_ENCODING_MASK);
    }

    /**
     * Determines if the encoding has been initialized.<br>
     *
     * It is useful to determine if a field is null on purpose or just because it has not been initialized.
     *
     * @return true if the field has been initialized, false otherwise
     */
    public boolean checkEncodingInitialized()
    {
        return 0 !=  (initialized & FL_STORE_ID_ENCODING_MASK);
    }
    /**
     * Getter method for {@link #data}.<br>
     * Meta Data Information (in progress):
     * <ul>
     * <li>full name: fl_store.data</li>
     * <li>comments: 二进制数据(最大16MB)</li>
     * <li>column size: 16777215</li>
     * <li>JDBC type returned by the driver: Types.LONGVARBINARY</li>
     * </ul>
     *
     * @return the value of data
     */
    @ThriftField(value=6)
    @JsonProperty("data")
    public java.nio.ByteBuffer getData(){
        return data;
    }
    /**
     * Setter method for {@link #data}.<br>
     * The new value is set only if equals() says it is different,
     * or if one of either the new value or the current value is null.
     * In case the new value is different, it is set and the field is marked as 'modified'.
     *
     * @param newVal the new value to be assigned to data
     */
    @ThriftField(name="data")
    @JsonProperty("data")
    public void setData(java.nio.ByteBuffer newVal)
    {
        modified |= FL_STORE_ID_DATA_MASK;
        initialized |= FL_STORE_ID_DATA_MASK;

        if (Objects.equals(newVal, data)) {
            return;
        }
        data = newVal;
    }
    /**
     * Determines if the data has been modified.
     *
     * @return true if the field has been modified, false if the field has not been modified
     */
    public boolean checkDataModified()
    {
        return 0 !=  (modified & FL_STORE_ID_DATA_MASK);
    }

    /**
     * Determines if the data has been initialized.<br>
     *
     * It is useful to determine if a field is null on purpose or just because it has not been initialized.
     *
     * @return true if the field has been initialized, false otherwise
     */
    public boolean checkDataInitialized()
    {
        return 0 !=  (initialized & FL_STORE_ID_DATA_MASK);
    }

    @Override
    public boolean beModified()
    {
        return 0 != modified;
    }
  
    @Override
    public boolean isModified(int columnID){
        return columnID>=0 && columnID < metaData.columnCount && 0 !=  (modified & (1 << columnID));
    }

    @Override
    public boolean isInitialized(int columnID){
        return columnID>=0 && columnID < metaData.columnCount && 0 !=  (initialized & (1 << columnID));
    }
    
    @Override
    public void resetIsModified()
    {
        modified = 0;
    }

    @Override
    public void resetPrimaryKeysModified()
    {
        modified &= (~(FL_STORE_ID_MD5_MASK));
    }
    /**
     * Resets columns modification status except primary keys to 'not modified'.
     */
    public void resetModifiedExceptPrimaryKeys()
    {
        modified &= (~(FL_STORE_ID_ENCODING_MASK |
            FL_STORE_ID_DATA_MASK));
    }

    /** reset all fields to initial value, equal to a new bean */
    public void reset(){
        this.md5 = null;
        this.encoding = null;
        this.data = null;
        this.isNew = true;
        this.modified = 0;
        this.initialized = 0;
    }

    @Override
    public StoreBean clone(){
        return (StoreBean) super.clone();
    }


    public static final Builder builder(){
        return new Builder().reset();
    }
    /** 
     * a builder for StoreBean,the template instance is thread local variable
     * a instance of Builder can be reused.
     */
    public static final class Builder{
        /** StoreBean instance used for template to create new StoreBean instance. */
        static final ThreadLocal<StoreBean> TEMPLATE = new ThreadLocal<StoreBean>(){
            @Override
            protected StoreBean initialValue() {
                return new StoreBean();
            }};
        private Builder() {}
        /** 
         * reset the bean as template 
         * @see StoreBean#reset()
         */
        public Builder reset(){
            TEMPLATE.get().reset();
            return this;
        }
        /** set a bean as template,must not be {@code null} */
        public Builder template(StoreBean bean){
            if(null == bean){
                throw new NullPointerException();
            }
            TEMPLATE.set(bean);
            return this;
        }
        /** return a clone instance of {@link #TEMPLATE}*/
        public StoreBean build(){
            return TEMPLATE.get().clone();
        }
        /** 
         * fill the field : fl_store.md5
         * @param md5 主键,md5检验码
         * @see StoreBean#getMd5()
         * @see StoreBean#setMd5(String)
         */
        public Builder md5(String md5){
            TEMPLATE.get().setMd5(md5);
            return this;
        }
        /** 
         * fill the field : fl_store.encoding
         * @param encoding 编码类型,GBK,UTF8...
         * @see StoreBean#getEncoding()
         * @see StoreBean#setEncoding(String)
         */
        public Builder encoding(String encoding){
            TEMPLATE.get().setEncoding(encoding);
            return this;
        }
        /** 
         * fill the field : fl_store.data
         * @param data 二进制数据(最大16MB)
         * @see StoreBean#getData()
         * @see StoreBean#setData(java.nio.ByteBuffer)
         */
        public Builder data(java.nio.ByteBuffer data){
            TEMPLATE.get().setData(data);
            return this;
        }
    }
}
