package net.gdface.facelog.client.thrift;

import com.facebook.swift.codec.*;
import com.facebook.swift.codec.ThriftField.Requiredness;
import java.util.*;

import static com.google.common.base.Objects.toStringHelper;

@ThriftStruct("MatchEntry")
public final class MatchEntry
{
    public MatchEntry() {
    }

    private String key;

    @ThriftField(value=1, name="key", requiredness=Requiredness.OPTIONAL)
    public String getKey() { return key; }

    @ThriftField
    public void setKey(final String key) { this.key = key; }

    private int pk;

    @ThriftField(value=2, name="pk", requiredness=Requiredness.REQUIRED)
    public int getPk() { return pk; }

    @ThriftField
    public void setPk(final int pk) { this.pk = pk; }

    @Override
    public String toString()
    {
        return toStringHelper(this)
            .add("key", key)
            .add("pk", pk)
            .toString();
    }
}
