package net.gdface.facelog.client.thrift;

import com.facebook.swift.codec.*;

public enum TmpPwdTargetType
{
    USER_GROUP_ID(0), DEVICE_GROUP_ID(1), USER_ID(2), DEVICE_ID(3);

    private final int value;

    TmpPwdTargetType(int value)
    {
        this.value = value;
    }

    @ThriftEnumValue
    public int getValue()
    {
        return value;
    }
}
