package net.gdface.facelog.client.thrift;

import com.facebook.swift.codec.*;
import com.facebook.swift.codec.ThriftField.Requiredness;
import java.util.*;

import static com.google.common.base.Objects.toStringHelper;

@ThriftStruct("LogLightBean")
public final class LogLightBean
{
    public LogLightBean() {
    }

    private boolean New;

    @ThriftField(value=1, name="_new", requiredness=Requiredness.REQUIRED)
    public boolean isNew() { return New; }

    @ThriftField
    public void setNew(final boolean New) { this.New = New; }

    private int modified;

    @ThriftField(value=2, name="modified", requiredness=Requiredness.REQUIRED)
    public int getModified() { return modified; }

    @ThriftField
    public void setModified(final int modified) { this.modified = modified; }

    private int initialized;

    @ThriftField(value=3, name="initialized", requiredness=Requiredness.REQUIRED)
    public int getInitialized() { return initialized; }

    @ThriftField
    public void setInitialized(final int initialized) { this.initialized = initialized; }

    private Integer id;

    @ThriftField(value=4, name="id", requiredness=Requiredness.OPTIONAL)
    public Integer getId() { return id; }

    @ThriftField
    public void setId(final Integer id) { this.id = id; }

    private Integer personId;

    @ThriftField(value=5, name="personId", requiredness=Requiredness.OPTIONAL)
    public Integer getPersonId() { return personId; }

    @ThriftField
    public void setPersonId(final Integer personId) { this.personId = personId; }

    private String name;

    @ThriftField(value=6, name="name", requiredness=Requiredness.OPTIONAL)
    public String getName() { return name; }

    @ThriftField
    public void setName(final String name) { this.name = name; }

    private Integer papersType;

    @ThriftField(value=7, name="papersType", requiredness=Requiredness.OPTIONAL)
    public Integer getPapersType() { return papersType; }

    @ThriftField
    public void setPapersType(final Integer papersType) { this.papersType = papersType; }

    private String papersNum;

    @ThriftField(value=8, name="papersNum", requiredness=Requiredness.OPTIONAL)
    public String getPapersNum() { return papersNum; }

    @ThriftField
    public void setPapersNum(final String papersNum) { this.papersNum = papersNum; }

    private Integer deviceId;

    @ThriftField(value=9, name="deviceId", requiredness=Requiredness.OPTIONAL)
    public Integer getDeviceId() { return deviceId; }

    @ThriftField
    public void setDeviceId(final Integer deviceId) { this.deviceId = deviceId; }

    private Integer verifyType;

    @ThriftField(value=10, name="verifyType", requiredness=Requiredness.OPTIONAL)
    public Integer getVerifyType() { return verifyType; }

    @ThriftField
    public void setVerifyType(final Integer verifyType) { this.verifyType = verifyType; }

    private Long verifyTime;

    @ThriftField(value=11, name="verifyTime", requiredness=Requiredness.OPTIONAL)
    public Long getVerifyTime() { return verifyTime; }

    @ThriftField
    public void setVerifyTime(final Long verifyTime) { this.verifyTime = verifyTime; }

    private Integer direction;

    @ThriftField(value=12, name="direction", requiredness=Requiredness.OPTIONAL)
    public Integer getDirection() { return direction; }

    @ThriftField
    public void setDirection(final Integer direction) { this.direction = direction; }

    private Double temperature;

    @ThriftField(value=13, name="temperature", requiredness=Requiredness.OPTIONAL)
    public Double getTemperature() { return temperature; }

    @ThriftField
    public void setTemperature(final Double temperature) { this.temperature = temperature; }

    private String imageMd5;

    @ThriftField(value=14, name="imageMd5", requiredness=Requiredness.OPTIONAL)
    public String getImageMd5() { return imageMd5; }

    @ThriftField
    public void setImageMd5(final String imageMd5) { this.imageMd5 = imageMd5; }

    private String props;

    @ThriftField(value=15, name="props", requiredness=Requiredness.OPTIONAL)
    public String getProps() { return props; }

    @ThriftField
    public void setProps(final String props) { this.props = props; }

    @Override
    public String toString()
    {
        return toStringHelper(this)
            .add("New", New)
            .add("modified", modified)
            .add("initialized", initialized)
            .add("id", id)
            .add("personId", personId)
            .add("name", name)
            .add("papersType", papersType)
            .add("papersNum", papersNum)
            .add("deviceId", deviceId)
            .add("verifyType", verifyType)
            .add("verifyTime", verifyTime)
            .add("direction", direction)
            .add("temperature", temperature)
            .add("imageMd5", imageMd5)
            .add("props", props)
            .toString();
    }
}
