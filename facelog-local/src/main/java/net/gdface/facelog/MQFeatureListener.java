package net.gdface.facelog;

import static com.google.common.base.Preconditions.*;

import gu.simplemq.IMessageQueueFactory;
import gu.simplemq.IPublisher;
import gu.simplemq.MessageQueueFactorys;
import gu.sql2java.TableListener;
import net.gdface.facelog.db.FeatureBean;

/**
 * 特征表({@code fl_feature})变动侦听器<br>
 * 当{@code fl_feature}记录增删改时发布订阅消息
 * @author guyadong
 *
 */
class MQFeatureListener extends TableListener.Adapter<FeatureBean> implements ChannelConstant{

	private final IPublisher publisher;
	public MQFeatureListener() {
		this(MessageQueueFactorys.getDefaultFactory());
	}
	public MQFeatureListener(IMessageQueueFactory factory) {
		this.publisher = checkNotNull(factory,"factory is null").getPublisher();
	}
	@Override
	public void afterInsert(FeatureBean bean) {
		new PublishTask<String>(
				PUBSUB_FEATURE_INSERT, 
				bean.getMd5(), 
				publisher)
		.execute();
	}
	@Override
	public void afterDelete(FeatureBean bean) {
		FeatureBean deleted = bean.clone();		
		// 为减少发送到redis数据体积，将特征码字段清除
		int modified = deleted.getModified();
		deleted.setFeature(null);
		deleted.setModified(modified);
		new PublishTask<FeatureBean>(
				PUBSUB_FEATURE_DELETE, 
				deleted, 
				publisher)
		.execute();		
	}			

}
