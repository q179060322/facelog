package net.gdface.facelog;

import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.base.Preconditions.checkState;

import gu.simplemq.IMessageQueueFactory;
import gu.simplemq.IPublisher;
import gu.simplemq.MessageQueueFactorys;
import gu.sql2java.TableListener;
import gu.sql2java.exception.RuntimeDaoException;
import net.gdface.facelog.db.PermitBean;

/**
 * 通行权限关联表({@code fl_permit})变动侦听器<br>
 * 当{@code fl_permit}记录增删改时发布订阅消息
 * @author guyadong
 *
 */
class MQPermitListener extends TableListener.Adapter<PermitBean> implements ChannelConstant{

	private final IPublisher publisher;
	private final BaseDao dao;
	private PermitBean beforeUpdatedBean;
	public MQPermitListener(BaseDao dao) {
		this(dao, MessageQueueFactorys.getDefaultFactory());
	}
	public MQPermitListener(BaseDao dao, IMessageQueueFactory factory) {
		this.dao = checkNotNull(dao,"dao is null");
		this.publisher = checkNotNull(factory,"factory is null").getPublisher();
	}
	@Override
	public void afterInsert(PermitBean bean) {
		new PublishTask<PermitBean>(
				PUBSUB_PERMIT_INSERT, 
				bean, 
				publisher)
		.execute();
	}

	@Override
	public void beforeUpdate(PermitBean bean) throws RuntimeDaoException {
		// 保留更新前的数据
		beforeUpdatedBean = dao.daoGetPermit(bean.getDeviceGroupId(), bean.getPersonGroupId()).clone();
	}
	@Override
	public void afterUpdate(PermitBean bean) throws RuntimeDaoException {
		// beforeUpdatedBean 为 null，只可能因为侦听器是被异步调用的
		checkState(beforeUpdatedBean != null,"beforeUpdatedBean must not be null");
		// 保存修改信息
		beforeUpdatedBean.setModified(bean.getModified());
		new PublishTask<PermitBean>(
				PUBSUB_PERMIT_UPDATE, 
				beforeUpdatedBean, 
				publisher)
		.execute();
		beforeUpdatedBean = null;
	}
	@Override
	public void afterDelete(PermitBean bean) {
		new PublishTask<PermitBean>(
				PUBSUB_PERMIT_DELETE, 
				bean, 
				publisher)
		.execute();
	}			

}
