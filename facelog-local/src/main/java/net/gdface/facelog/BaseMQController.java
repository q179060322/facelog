package net.gdface.facelog;

import java.io.IOException;
import java.lang.ProcessBuilder.Redirect;
import java.util.List;
import com.google.common.base.Joiner;
import gu.simplemq.IMessageQueueFactory;
import static com.google.common.base.Preconditions.*;
/**
 * 本地消息服务控制器基类<br>
 * @author guyadong
 *
 */
abstract class BaseMQController extends BaseServiceController {

	protected IMessageQueueFactory factory;
	BaseMQController(IMessageQueueFactory factory) {
		this.factory = factory;
	}
	@Override
	protected boolean testConnect(){
		return checkNotNull(factory, "factory is uninitialized").testConnect();
	}

	/**
	 * 当为非嵌入式服务时({@link #isEmbedded()}返回false)必须重写此方法
	 * @return 返回启动服务的程序名及命令行参数列表
	 */
	protected List<String> makeStartCommand(){
		throw new UnsupportedOperationException();
	}
	
	@Override
	protected Process startLocalServer() {
		String name = serviceName();
		List<String> args = makeStartCommand();

		try {
			logger.info("start {} server(启动{}服务器) {}",name,name,	location);
			
			String cmd = Joiner.on(' ').join(args);
			logger.info("cmd(启动命令): {}",cmd);
			return new ProcessBuilder(args)
					.redirectError(Redirect.INHERIT)
					.redirectOutput(Redirect.INHERIT)
					.start();
		} catch (IOException e) {
			throw new RuntimeException(e);
		}	
	}

}
