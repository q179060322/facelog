package net.gdface.facelog;

import static org.junit.Assert.*;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import org.junit.Test;

import com.google.common.base.Strings;
import com.google.common.hash.Hashing;

import net.gdface.facelog.db.PersonBean;

public class TokenTest implements ServiceConstant{

	@Test
	public void test1ApplyRootToken() {
		IFaceLog instance = new FaceLogImpl();
		try {
			String password = CONFIG.getString(ROOT_PASSWORD);
			String passwordMd5 = Hashing.md5().hashBytes(password.getBytes()).toString();
			Token token = instance.applyRootToken(passwordMd5, true);
			logger.info(token.toString());
		} catch (ServiceSecurityException e) {
			e.printStackTrace();
		}
	}
	@Test
	public void test2SavePerson(){
		IFaceLog instance = new FaceLogImpl();
		String password = "do you know ?";
		String passwordMd5 = Hashing.md5().hashBytes(password.getBytes()).toString();
		PersonBean person = PersonBean.builder().name("顾亚东").password(passwordMd5).build();
		instance.savePerson(person, null);
		logger.info(person.toString(true, false));
		assertTrue("password check not pass",instance.isValidPassword(Integer.toString(person.getId()), password, true));
	}
	
	private static Date toDate(String date) throws ParseException{
		if(Strings.isNullOrEmpty(date)){
			return null;
		}
		try {
			return new SimpleDateFormat(ISO8601_FORMATTER_STR).parse(date);
		} catch (ParseException e) {
			try {
				return new SimpleDateFormat(TIMESTAMP_FORMATTER_STR).parse(date);
			} catch (ParseException e2) {
				return new SimpleDateFormat(DATE_FORMATTER_STR).parse(date);
			}
		}
	}
	
	@Test
	public void test3ToDate(){
		try {
			Date d = toDate("2020-06-06T11:07:18.066Z");
			//Date d = toDate("2020-06-01T10:00:00.000Z");
			Date now = new Date();
			System.out.printf("d:\t%s\nnow:\t%s\n", d,now);
			SimpleDateFormat format = new SimpleDateFormat(ISO8601_FORMATTER_STR);
			System.out.printf("now iso8601:%s", format.format(now));
		} catch (ParseException e) {
			// TODO 自动生成的 catch 块
			e.printStackTrace();
		}
	}
	@SuppressWarnings("deprecation")
	@Test
	public void test4ToDate() throws ParseException{
		SimpleDateFormat format = new SimpleDateFormat(ISO8601_FORMATTER_STR);
//		TimeZone zone = TimeZone.getTimeZone("UTC");
//		TimeZone zone = TimeZone.getDefault();
//		format.setTimeZone(	zone);
//		System.out.printf("zone %s",TimeZone.getDefault());
		Date now = new Date();
		System.out.printf("now date:\t%s GMT\n", now.toGMTString());
		System.out.printf("now date:\t%s LOCALE\n", now.toLocaleString());

		String nowstr = format.format(now);
		System.out.printf("now format:\t%s\n",nowstr);
		System.out.printf("parsed now:\t%s LOCALE\n",toDate(nowstr).toLocaleString());
		{
			System.out.printf("f2 parsed now:\t%s LOCALE\n",toDate(nowstr).toLocaleString());
		}
	}

}
